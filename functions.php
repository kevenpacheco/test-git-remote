<?php
/*
  Plugin Name: Test Git Remote
  Version: 1.0.11
  GitLab Plugin URI: kevenpacheco/test-git-remote
  Primary Branch: main
*/

(function () {
  if (function_exists('acf_register_block_type')) {
    $name = 'Test Git Remote';

    acf_register_block_type([
      'name'              => $name,
      'title'             => $name,
      'description'       => $name,
      'render_template'   => plugin_dir_path(__FILE__) . 'html.php',
      'category'          => 'formatting',
      'icon'              => 'smartphone',
      'keywords'          => ['skallar'],
    ]);
  }
})();
